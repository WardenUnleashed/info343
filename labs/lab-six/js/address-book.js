/* address-book.js
    this is where you will add your JavaScript to complete Lab 5
*/

$(document).ready( function () {
    getElements();
    render();
});

function getElements() {
    //get the template
    Employees.entries.template = $('#template').html();
    //get the container
    Employees.entries.$container = $('.address-book');
}

function render() {
    //templated_data will be the template string after underscore templates our data
    var templated_data = _.template(Employees.entries.template, Employees.entries);
    //now put it in the container
    Employees.entries.$container.html(templated_data);
}