/* address-book.js
    this is where you will add your JavaScript to complete Lab 5
*/

var contactsInformation = [];
$(document).ready( function () {
 makeBlanks();
 sortObjArray(Employees.entries, 'last')
 populate();
 render();
});

function makeBlanks(){
	var element = $('div.address-book').html();
	for (var i = 0; i < Employees.entries.length; i++){
 		contactsInformation.push($(element)); //put newly made jQuery object into the array
 	}
	var element = $('div.address-book').html('');	
}

	
function populate() {
	for(i = 0; i < contactsInformation.length; i++){
		contactsInformation[i].find('.pic').attr("src", Employees.entries[i].pic);
		contactsInformation[i].find('.name').text( Employees.entries[i].first + ' ' +  Employees.entries[i].last);
		contactsInformation[i].find('.title').text( Employees.entries[i].title);
		contactsInformation[i].find('.dept').text( Employees.entries[i].dept);
	}
	
}

function render() {
	for(i = 0; i < contactsInformation.length; i++){
		 $('div.address-book').append(contactsInformation[i]);
	}
	 $(".person").removeClass("template");
}

$( '.sort-ui' ).click(function() {
	var $sortBtn = $(this);
	sortObjArray(Employees.entries, $sortBtn.attr('data-sortby'));
 	populate();
 	render();
	$sortBtn.siblings().removeClass("active");
	$sortBtn.addClass("active");
	
});

/* sortObjArray()
    sorts an array of objects by a given property name
    the property values are compared using standard 
    operators, so this will work for string, numeric,
    boolean, or date values

    objArray        array of objects to sort
    propName        property name to sort by

    returns undefined (array is sorted in place)
*/
function sortObjArray(objArray, propName) {
    if (!objArray.sort)
        throw new Error('The objArray parameter does not seem to be an array (no sort method)');

    //sort the array supplying a custom compare function
    objArray.sort(function(a,b) {
        
        //note: this compares only one property of the objects
        //see the optional step where you can add support for 
        //a secondary sort key (i.e., sort by another property)
        //if the first property values are equal
        if (a[propName] < b[propName])
            return -1;
        else if (a[propName] === b[propName])
            return 0;
        else
            return 1;
    });
} //sortObjArray()

