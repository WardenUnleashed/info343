$( document ).ready(function() {
	        var mapOptions = {
          center: new google.maps.LatLng(47.6531361, -122.3061275),
          zoom: 17
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
	google.maps.event.addDomListener(window, 'load', map);
	var cafeInfo = $.getJSON( "http://128.208.132.98/html/class/spots.php?campus=seattle", function() {
	  console.log( "success" );
	})
	  .done(function() {
		console.log( "second success" );
	  })
	  .fail(function() {
		console.log( "error" );
	  }) 
	// Perform other work here ...
	 
	// Set another completion function for the request above
	cafeInfo.complete(function() {
	  	
		var markers = [];
			for(var i = 0; i < cafeInfo.responseJSON.length; i++){
				var marker = createMarker(cafeInfo.responseJSON[i], map);
				marker.infoWindow = createDescription(cafeInfo.responseJSON[i], map);
				
				markers.push(marker);
				google.maps.event.addListener(markers[i], 'click', function() { this.infoWindow.open(map,this); });
			}
			drop(markers, map);
	});
	
	function clear() {
	  for (var i =0; i < markers.length; i++) {
		  markers[i].setMap(null);
	
	  }
	  markers = null;
	}
	

});




 function initialize(map) {

 }
 
 function createMarker(object, map) {
	 var marker = new google.maps.Marker({
		position: new google.maps.LatLng(object.location.latitude,object.location.longitude),
		map: map,
		title:object.name
	});
	return marker;
 }
 
 function createDescription(object, map){
	var contentString = '<div id="content">'+
		  '<div class="siteNotice"> + </div>' +
		  '<h1 id="firstHeading" class="firstHeading">' + object.name + '</h1>'+
		  '<div id="bodyContent">'+
		  '<p>'+object.location.description+'</p>'+
		  '<p>'+ object["hours-today"] + '</p>'+
		  '<img src="'+ object.thumbnail +'" class="infoPic">'+
		  
		  '</div>'+
		  '</div>';

  var infowindow = new google.maps.InfoWindow({
      content: contentString
 	});
	return infowindow
 }
      
function drop(markers, map) {
  for (var i =0; i < markers.length; i++) {
      markers[i].setMap(map);;

  }
}


	  