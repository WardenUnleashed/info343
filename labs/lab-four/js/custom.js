// JavaScript Document

$( document ).ready(function() {
    $('#animation').on('mouseover', animate);
	$('#animation').on('mouseout', stopMan);
});

var count = 2;
var running; 
function animate() {
		$("#animation").css("border", "1px red solid");
		running = setInterval(advance, 100);
}
function stopMan() {
	$("#animation").css("border", "none");
	clearInterval(running);
}

function advance() {
	if(count == 30){
		$('#animation').removeClass();
		$('#animation').addClass('frame frame1');
		count=2;		
	}else{
		$('#animation').removeClass();
		$('#animation').addClass('frame frame' + count);
		count++;
	}
};

