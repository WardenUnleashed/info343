<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <title>Dawg Pizza</title>
   <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css"/>
   <style>
    body{
    background-color:#faa;
    }
    .block2{
        /*display:none;*/
    }
    .container{
      background-color:#fff;
      margin-top:5px;
      padding-bottom:10px;
    }
    }
    </style>
</head>
<body>
  <div class="container">
    <div class="order">
        <div class="alert alert-warning alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
           <strong>Warning!</strong> Better check yourself, and try again.
        </div>
        <div class="row">
          <div class="col-xs-12">
            <h1>Dawg Pizza Online Ordering System</h1>
            <h2>Delivery within Seattle City Limits, 12:00pm - 11:00pm</h2>
        </div>
      </div>
      <div class="row block1">
          <div class="col-xs-12">
            <form role="form" id="contact-info">
              <div class="form-group">
                <label for="first-name">First Name</label>
                <input type="text" class="form-control" name="first" placeholder="Bob">
              </div>
              <div class="form-group">
                <label for="last-name">Last Name</label>
                <input type="text" class="form-control" name="last" placeholder="Smith">
              </div>
              <div class="form-group">
                <label for="phone">Cell Phone</label>
                <input type="tel" class="form-control" name="phone" placeholder="(xxx)-xxx-xxxx">
              </div>
              <div class="form-group">
                <label for="address-line1">Address</label>
                <input type="text" class="form-control" name="address1" placeholder="1234 University Way NE">
                <input type="text" class="form-control" name="address2" placeholder="">
              </div>
              <div class="form-group">
                <label for="zip">Zip Code</label>
                <input type="text" class="form-control" name="zip" placeholder="98105">
              </div>
              <button type="submit" class="btn btn-primary">Next</button>
            </form>
          </div>
      </div>
      <div class="row block2">
          <form role="form" id="order-info">
          <div class="col-xs-8">
            <h2>Pizza</h2>
            <table class="table">
              <th>
                <td>Item</td>
                <td>Quantity</td>
              </th>
          </div>
          <div class="col-xs-4">
              <h2> Cart Checkout </h2>
              <div class="cart">
              </div>
          </div>
          </form>
      </div>
    </div>
</div>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="js/bootstrap.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js"></script>
   <script src="js/menu.js"></script>
  <script src="js/custom.js"></script>
  <script>
$(document).ready(function() {
  var jsonObj = {
      "name": "",
      "address1": "",
      "address2": "",
      "zip": "",
      "phone": "",
      "nextUrl": "",
      "nextCaption": "",
      "items": []
  }
   var pizzaTemplate = _.template(
            $( "script#pizza-template" ).html()
        );
    
           $(".table" ).html(
            pizzaTemplate(com.dawgpizza.menu.pizzas)
        );
  $('.alert').hide();
  var validatorInfo = $('#contact-info').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        first: {
            message: 'Please Enter Your Last Name',
            validators: {
                notEmpty: {
                    message: 'Your First Name is Required'
                },
                stringLength: {
                    min: 2,
                    max: 30,
                    message: 'Lets Have a Real Name :D'
                }
            }
        },
        last: {
            message: 'Please Enter Your Last Name',
            validators: {
                notEmpty: {
                    message: 'Your Last Name is Required'
                },
                stringLength: {
                    min: 2,
                    max: 30,
                    message: 'Lets Have a Real Name :D'
                }
            }
        },
        phone: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Your Phone Number'
                },
                phone: {
                    message: 'Please input a valid phone number',
                    country: 'US'
                }
            }
        },
        address1: {
            validators: {
                notEmpty: {
                    message: 'Please Enter an Address for Delivery'
                },
            }
        },
        zip: {
            validators: {
                notEmpty: {
                    message: 'Please Enter a Zip Code'
                },
                zipCode: {
                  country: 'US',
                  message: 'Please Enter Your Zip Code'
                }
            }
        }
    }
  });
  validatorInfo.on('success.form.bv', function(e) {
    e.preventDefault()
    // Get the form instance
    var $form = $(e.target);
    // Get the BootstrapValidator instance
    var bv = $form.data('bootstrapValidator');
    if(bv.isValid()){
      jsonObj.name = $('[name="first"]').val() + ' ' + $('[name="last"]').val();
      jsonObj.address1 = $('[name="address1"]').val();
      jsonObj.address2 = $('[name="address2"]').val();
      jsonObj.address1 = $('[name="address1"]').val();
      jsonObj.zip = $('[name="zip"]').val();
      jsonObj.phone = $('[name="phone"]').val();
      console.log(jsonObj);
      $('.block1').fadeOut();
      $('.block2').delay(800).fadeIn();
    }
  });
});

function updateCart(){
    $(.cart).html('');
    for(var i = 0; i < jsonObj.items.length;i++){
      $(.cart).append(jsonObj.items[i]['size'] +  jsonObj.items[i]['name'] + 'x ' + jsonObj.items[i].quantity);
    }
  }

  </script>
</body>
</html>