$( document ).ready(function() {

       var meatTemplate = _.template(
            $( "script#meat-template" ).html()
        );

		       $(".meat" ).append(
            meatTemplate(com.dawgpizza.menu.pizzas)
        );

		 var cart = {
		      "name": "",
		      "address1": "",
		      "address2": "",
		      "zip": "",
		      "phone": "",
		      "nextUrl": "",
		      "nextCaption": "",
		      "items": []
		  }
		  $('.alert').hide();
  var validatorInfo = $('#contact-info').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
    },
    fields: {
        first: {
            message: 'Please Enter Your Last Name',
            validators: {
                notEmpty: {
                    message: 'Your First Name is Required'
                },
                stringLength: {
                    min: 2,
                    max: 30,
                    message: 'Lets Have a Real Name :D'
                }
            }
        },
        last: {
            message: 'Please Enter Your Last Name',
            validators: {
                notEmpty: {
                    message: 'Your Last Name is Required'
                },
                stringLength: {
                    min: 2,
                    max: 30,
                    message: 'Lets Have a Real Name :D'
                }
            }
        },
        phone: {
            validators: {
                notEmpty: {
                    message: 'Please Enter Your Phone Number'
                },
                phone: {
                    message: 'Please input a valid phone number',
                    country: 'US'
                }
            }
        },
        address1: {
            validators: {
                notEmpty: {
                    message: 'Please Enter an Address for Delivery'
                },
            }
        },
        zip: {
            validators: {
                notEmpty: {
                    message: 'Please Enter a Zip Code'
                },
                zipCode: {
                  country: 'US',
                  message: 'Please Enter Your Zip Code'
                }
            }
        }
    }
  });
  validatorInfo.on('success.form.bv', function(e) {
    e.preventDefault()
    // Get the form instance
    var $form = $(e.target);
    // Get the BootstrapValidator instance
    var bv = $form.data('bootstrapValidator');
    if(bv.isValid()){
      cart.name = $('[name="first"]').val() + ' ' + $('[name="last"]').val();
      cart.address1 = $('[name="address1"]').val();
      cart.address2 = $('[name="address2"]').val();
      cart.address1 = $('[name="address1"]').val();
      cart.zip = $('[name="zip"]').val();
      cart.phone = $('[name="phone"]').val();
for(var i; i < com.dawgpizza.menu.pizzas.length; i++){
    	if($('#quantity' + (i + 1)).val() > 0){
    		cart.items.push({
    				name:  com.dawgpizza.menu.pizzas[i].name,
    				type: 'pizza',
    				size: $('#size' + (i + 1)).val(),
    				quantity: $('#quantity' + (i + 1)).val()
    		});
    	}
    }
    var jsonObject = $(cart).serialize();
  }
  
        $.ajax({
            url: 'http://dawgpizza.com/orders/',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                window.alert('Pizza is On Its Way');
            },
            data: jsonObject
        });
});



});
