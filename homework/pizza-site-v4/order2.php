<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <title>Dawg Pizza</title>
   <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/css/bootstrapValidator.min.css"/>
   <link rel="stylesheet" href="css/order.css"/>
</head>
<body>
	<div class="container">
        <div class="row">
       		 <div class="col-xs-12 text-center">
          		<img src="img/logo.png">
            	<h1>Dawg Pizza Online Ordering System</h1>
            	<h2>Delivery within Seattle City Limits, 12:00pm - 11:00pm</h2>
        	</div>
		</div>
		<hr>
		<div class="row block1">
          <div class="col-xs-12">
            <form role="form" id="contact-info">
              <div class="form-group">
                <label for="first-name">First Name</label>
                <input type="text" class="form-control" name="first" placeholder="Bob">
              </div>
              <div class="form-group">
                <label for="last-name">Last Name</label>
                <input type="text" class="form-control" name="last" placeholder="Smith">
              </div>
              <div class="form-group">
                <label for="phone">Cell Phone</label>
                <input type="tel" class="form-control" name="phone" placeholder="(xxx)-xxx-xxxx">
              </div>
              <div class="form-group">
                <label for="address-line1">Address</label>
                <input type="text" class="form-control" name="address1" placeholder="1234 University Way NE">
                <input type="text" class="form-control" name="address2" placeholder="">
              </div>
              <div class="form-group">
                <label for="zip">Zip Code</label>
                <input type="text" class="form-control" name="zip" placeholder="98105">
              </div>
          </div>
      	</div>
      	<hr>
     	 <div class="row block1">
     		<form role="form" id="order-info">
      			<div class="col-md-12">
        			<h2>Pizza</h2>
        			<table class="meat table">
        							<tr>
				               			<th>Pizza</th>
				               			<th>Description</th>
				               			<th>Size</th>
				               			<th>Quantity</th>
				               		</tr>
								<script id='meat-template' type='text/template'>

				               		<% for(var i = 0; i < com.dawgpizza.menu.pizzas.length; i++) {%>
										
										<tr>
											<td class="name"><%= com.dawgpizza.menu.pizzas[i].name %></td>
											<td><%= com.dawgpizza.menu.pizzas[i].description %></td>
											<td class="size">
												<div class="form-group">
												<select class="form-control" id="size<%=i + 1 %>">
													  <option value="small">small</option>
													  <option value="medium">medium</option>
													  <option value="large">large</option>
												</select>
												</div>
											</td>
											<td><div class="form-group">
												<input type="number" class="form-control" id="quantity<%= i + 1 %>" value="0">
												</div>
											</td>
										</tr>
										
									<% } %>
								</script>
						</script>
      				</table>
      			</div>
      			<button type="submit" class="btn btn-primary">Order Up</button>
      		</form>
      	</div>
	</div>
 <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
 <script src="js/bootstrap.min.js" type="text/javascript"></script>
 <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.0/js/bootstrapValidator.min.js"></script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js"></script>
   <script src="http://www.dawgpizza.com/orders/menu.js"></script>
 <script src="js/order.js" type="text/javascript"></script>
</body>
</html>
