<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <title>Dawg Pizza</title>
   <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="css/style.css" rel="stylesheet" type="text/css" />
   <link href="css/about.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>
<body>
   <div class="overlay"></div>
   	<?php include 'header.php';?>
  <div class="container">
        <div class="row">
         <div class="col-xs-12" id="headline">
            <h1>About Dawg Pizza</h1>
         </div>
      </div>
  <div class="over">
      <div class="row">
         <div class="col-xs-12">
            <h2>Our Story</h2>
            <hr>
            <p>Dawg Pizza was started in <span class="accent">2013</span> with one simple goal: to get the students of the University of Washington to eat our pizza for every single meal. Well, maybe not every single meal, but most of them anyway. Cold pizza is really good for breakfast. And who can deny that pizza for lunch is really tasty? And when you've had a long day of classes and you're hungry, why not order a pizza for dinner too? And what should you do when you get hungry during that late-night study session? Order a pizza! <span class="accent">That's where we come in.</span></p>
            <p>We make the traditional pizzas, but we also mix things up with non-traditional ingredients. Tomato sauce is great, but how about plum, peanut, soy, or teriyaki sauce? You can get pepperoni anywhere, so how about some roasted duck or venison instead? Mushrooms are nice, but what about stir-fried cabbage and fried egg? <span class="accent">We do it all.</span></p>
            <p>Our pizzas are the <span class="accent">best</span> in Seattle, but don't take our word for it. <span class="accent">Try one and you'll see!</span></p>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12">
            <h2>Our Ecological Impact:</h2>
            <hr>
            <p>We love the environment, and we take extra steps to reduce our ecological footprint. For example, we collect and burn only naturally fallen wood from happy trees located within 100 miles of the restaurant. All our ingredients are <span class="accent">fair-trade, organic, and residents of Washington State</span>. Our meats come from animals that were named, raised in the ranchers' homes as if they were their children, and educated in the finest animal universities. The wheat for our dough is crushed by hand using a mortar and pestle. Our pizza boxes are made from <span class="accent">recycled</span> exam Blue Books and are <span class="accent">guaranteed</span> to decompose within seconds of finishing your pizza. We deliver using only rodent-powered vehicles and we all bike to work. <span class="accent">We love the environment!</span></p>
         </div>
      </div>
    </div>
   </div>
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type=
      "text/javascript"></script> 
   <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>