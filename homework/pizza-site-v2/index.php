<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <title>Dawg Pizza</title>
   <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="css/style.css" rel="stylesheet" type="text/css" />
   <link href="css/index.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>
<body>
   <div class="overlay"></div>
	<?php include 'header.php';?>
        <div class="container">
      <div class="row">
         <div class="col-xs-12" id="headline">
            <h1>Dawg Pizza</h1>
         </div>
      </div>
      <div class="row">
         <div class="col-xs-12" id="subline">
            Awesome <span class="accent">wood-fired</span> pizza with
            <span class="accent">fresh</span> ingredients baked to order.<br />
            Come in and eat, or call us for delivery!
         </div>
      </div>
      <div class="row space">
         <div class="col-md-6" id="map_canvas"></div>
         <div class="col-md-6" id="contact">
            <strong>Hours:</strong> 10:00am - 11:00pm. Sat - Sun<br />
            <strong>Phone:</strong> 206 - 472 - 4587<br />
            <strong>Address:</strong> 4576 University Ave NE, Seattle, WA 98105<br />
            <div class="row" id="socialMedia">
               <div class="col-xs-6" id="twitter">
                  <strong>Twitter</strong><br />
                  <a href="http://www.twitter.com/DawgPizzsa">@DawgPizza</a>
               </div>
               <div class="col-xs-6" id="email">
                  <strong>E-mail</strong><br />
                  <a href="mailto:tony@dawgpizza.com">Tony@DawgPizza.com</a>
               </div>
            </div>
         </div>
      </div>
      <div class="row space">
         <div class="col-lg-6 jobs">
            <h1>Employees Wanted</h1>
            <button type="button" class=
               "btn btn-default btn-lg" data-toggle="modal" data-target="#jobApp">Click to Apply</button>
         </div>
         <div class="col-lg-6 tubeyou">
            <iframe class="youtube" src=
               "http://www.youtube.com/embed/HWL__9yDu8I?autohide=1&amp;autoplay=0&amp;cc_load_policy=0&amp;controls=0&amp;disablekb=0&amp;fs=1&amp;iv_load_policy=3&amp;loop=0&amp;modestbranding=1&amp;origin=0&amp;rel=0&amp;showinfo=0&amp;theme=0"
               ></iframe>
         </div>
      </div>
   </div>
   <div class="modal fade" id="jobApp">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal"><span aria-hidden=
                  "true">&#215;</span><span class="sr-only">Close</span></button>
               <h4 class="modal-title">Employee Application</h4>
               <p>We are currently looking for drivers and kitchen staff, if you have any previous experience in these type of jobs please <strong>apply</strong>!</p>
            </div>
            <div class="modal-body">
               <form class="form-horizontal" role="form">
                  <div class="form-group">
                     <label for="inputName" class="col-sm-2 control-label">First & Last Name</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputName" placeholder="John Smith">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="inputEmail" class="col-sm-2 control-label">E-Mail</label>
                     <div class="col-sm-10">
                        <input type="email" class="form-control" id="inputEmail" placeholder="tony@dawgpizza.com">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="inputPhone" class="col-sm-2 control-label">Phone Number</label>
                     <div class="col-sm-10">
                        <input type="tel" class="form-control" id="inputPhone" placeholder="xxx-xxx-xxxx">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="inputExperience" class="col-sm-2 control-label">Work Experience</label>
                     <div class="col-sm-10">
                        <textarea class="form-control" rows="3" id="inputExperience"></textarea>
                     </div>
                  </div>
               </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss=
                  "modal">Close</button>
               <button type="button" class="btn btn-primary" id="submit" data-toggle="tooltip" data-placement="top" title="Application Accepted Soon">Submit Application</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- /.modal --> 
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type=
      "text/javascript"></script> 
   <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="js/bootstrap.min.js" type="text/javascript"></script> <script src="https://maps.googleapis.com/maps/api/js" type="text/javascript"></script> <script type="text/javascript">
      //<![CDATA[
            function initialize() {
              var map_canvas = document.getElementById('map_canvas');
              var map_options = {
                center: new google.maps.LatLng(47.662945, -122.313087),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
              }
              var map = new google.maps.Map(map_canvas, map_options)
      		var latLng = new google.maps.LatLng(47.662945, -122.313087)
      		var marker = new google.maps.Marker({
            position: latLng,
            map: map,
          });
            }
            google.maps.event.addDomListener(window, 'load', initialize);
          //]]>
      	
      	//Pops Up Job Application ToolTip
      	$(function(){
          	$('#submit').tooltip();
      
      	});
          
   </script>
</body>
</html>