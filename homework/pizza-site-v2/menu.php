<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <title>Dawg Pizza</title>
   <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="css/style.css" rel="stylesheet" type="text/css" />
   <link href="css/menu.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>
<body>
   <div class="overlay"></div>
  	<?php include 'header.php';?>
        <div class="container">
   <div class="row">
      <div class="col-xs-12" id="headline">
         <h1>Dawg Pizza Menu</h1>
      </div>
   </div>
   <div id="chalkboard">
      <div class="row">
         <div class="col-xs-12">
            <h2>Pizzas</h2>
            <p> All our pizzas are made with hand-thrown thin crust, baked in real wood ovens. Choose from one of these sizes:</p>
            <ul>
               <li>Small (12")</li>
               <li>Medium (14")</li>
               <li>Large (17")</li>
            </ul>
            
            <p class="accent text-center">All pizzas can be customized--add a few ingredients, or take a few off--for an extra $2.</p>
         </div>
      </div>
      <div class="row">
         <div class="col-md-6">
            <h2>Meat Pies</h2>
            <dl>
               <dt>Classic Pepperoni</dt>
               <dd>Pepperoni and Mozzarella on our Spicy Tomato Sauce. $10/$13/$16</dd>
               <dt>The Hawaiian</dt>
               <dd>Canadian Bacon and Pineapple with Mozzarella on a rich Tomato Sauce.  $12/$14/$17</dd>
               <dt>Duck, Duck, Goose</dt>
               <dd>Roasted Duck and Goose with Bacon, Chestnuts on a rich Plum Sauce.  $15/$17/$19</dd>
               <dt>The Bambi</dt>
               <dd>Slow-cooked Venison with Red Cabbage on our famous Black Cherry Sauce.  $15/$17/$19</dd>
               <dt>The Hawaiian</dt>
               <dd>Canadian Bacon and Pineapple with Mozzarella on a rich Tomato Sauce.  $12/$14/$17</dd>
               <dt>The Ultimate</dt>
               <dd>Pepperoni, Bacon, Canadian Bacon, Chicken, Duck, Goose, and Ground Beef with Smoked Mozzarella on our Spicy Tomato Sauce. Add Venison or Elk for an extra $2!  $15/$19/$23</dd>
            </dl>
         </div>
         <div class="col-md-6">
            <h2>Vegetarian Pies</h2>
            <dl>
               <dt>Margherita</dt>
               <dd>Mozzarella, Basil, Salt on an Olive Oil Base.  $10/$13/$16</dd>
               <dt> Veggie Madness</dt>
               <dd>Mushroom, Black Olive, Onions, Roasted Garlic, Squash and Roasted Eggplant on our Spicy Tomato Sauce.  $11/$14/$17</dd>
               <dt>Forest Floor</dt>
               <dd>Three kinds of mushrooms with Mozzarella on a herb Tomato Sauce.  $11/$14/$17</dd>
               <dt>Mr Green</dt>
               <dd>Roasted Tofu with Romano on a Basil Pesto sauce. $13/$16/$19</dd>
               <dt>Purple Monster</dt>
               <dd>Roasted Eggplant and Cabbage stir fried in sesame oil on a rich Plum Sauce.  $11/$14/$17</dd>
            </dl>
         </div>
      </div>
      <div class="row">
         <div class="col-md-6">
            <h2>Drinks</h2>
            <ul>
               <li>Coke, Diet Coke, Sprite, Root Beer, or Irn Bru by the can ($4)</li>
               <li>Rainier Beer by the can ($4)</li>
               <li>House Red Wine by the glass or bottle ($10/$40)</li>
            </ul>
         </div>
         <div class="col-md-6">
            <h2>Desserts</h2>
            <ul>
               <li>Chocolate Gelato ($8)</li>
               <li>Lemon Sorbet ($7)</li>
               <li>Ricotta Cheese Cake ($10)</li>
            </ul>
         </div>
      </div>
   </div>
</div>
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type=
      "text/javascript"></script> 
   <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>