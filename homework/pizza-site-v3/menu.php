<!DOCTYPE html>
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <title>Dawg Pizza</title>
   <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <link href="css/style.css" rel="stylesheet" type="text/css" />
   <link href="css/menu.css" rel="stylesheet" type="text/css" />
   <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
   <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
   <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
   <![endif]-->
</head>
<body>
   <div class="overlay"></div>
  	<?php include 'header.php';?>
        <div class="container">
   <div class="row">
      <div class="col-xs-12" id="headline">
         <h1>Dawg Pizza Menu</h1>
      </div>
   </div>
   <div id="chalkboard">
      <div class="row">
         <div class="col-xs-12">
            <h2>Pizzas</h2>
            <p> All our pizzas are made with hand-thrown thin crust, baked in real wood ovens. Choose from one of these sizes:</p>
            <ul>
               <li>Small (12")</li>
               <li>Medium (14")</li>
               <li>Large (17")</li>
            </ul>
            
            <p class="accent text-center">All pizzas can be customized--add a few ingredients, or take a few off--for an extra $2.</p>
         </div>
      </div>
      <div class="row">
         <div class="col-md-6">
        	<h2>Meat Pies</h2>
        	<dl class="meat">
				<script id='meat-template' type='text/template'>
               		<% for(var i = 0; i < com.dawgpizza.menu.pizzas.length; i++) {%>
						<% if(com.dawgpizza.menu.pizzas[i].vegetarian == null) { %>
						<dt><%= com.dawgpizza.menu.pizzas[i].name %></dt>
						<dd><%= com.dawgpizza.menu.pizzas[i].description %> <%= com.dawgpizza.menu.pizzas[i].prices[0] %> / <%= com.dawgpizza.menu.pizzas[i].prices[1] %> / <%= com.dawgpizza.menu.pizzas[i].prices[2] %> </dd>
						<% } %>
					<% } %>
				</script>
            </dl>
        </div>
    	<div class="col-md-6">
        	<h2 class="veg-title">Vegetarian Pies</h2>
        	<dl class="veg">
				<script id='veg-template' type='text/template'>
               		<% for(var i = 0; i < com.dawgpizza.menu.pizzas.length; i++) {%>
						<% if(com.dawgpizza.menu.pizzas[i].vegetarian != null) { %>
						<dt><%= com.dawgpizza.menu.pizzas[i].name %></dt>
						<dd><%= com.dawgpizza.menu.pizzas[i].description %> <%= com.dawgpizza.menu.pizzas[i].prices[0] %> / <%= com.dawgpizza.menu.pizzas[i].prices[1] %> / <%= com.dawgpizza.menu.pizzas[i].prices[2] %> </dd>
						<% } %>
					<% } %>
				</script>
            </dl>
        </div>
      </div>
      <div class="row">
         <div class="col-md-6">
            <h2>Drinks</h2>
            <ul>
               <li class="dranks"></li>
               <li>Rainier Beer by the can ($4)</li>
               <li>House Red Wine by the glass or bottle ($10/$40)</li>
            </ul>
         </div>
         <div class="col-md-6">
            <h2>Desserts</h2>
            <ul class="desserts">
            	<script id='dessert-template' type='text/template'>
					<% for(var i = 0; i < com.dawgpizza.menu.desserts.length; i++) {%>
               			<li><%=com.dawgpizza.menu.desserts[i].name %> ($<%=com.dawgpizza.menu.desserts[i].price %>)</li>
					<% } %>
				</script>
            </ul>
         </div>
      </div>
   </div>
</div>
   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type=
      "text/javascript"></script> 
   <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.6.0/underscore-min.js"></script>
   <script src="http://www.dawgpizza.com/orders/menu.js"></script>
	<script src="js/custom.js"></script>
</body>
</html>