// JavaScript Document
$( document ).ready(function() {


//Templating the pizza
       var vegTemplate = _.template(
            $( "script#veg-template" ).html()
        );
       var meatTemplate = _.template(
            $( "script#meat-template" ).html()
        );
       var dessertTemplate = _.template(
            $( "script#dessert-template" ).html()
        );
		
		       $(".meat" ).html(
            meatTemplate(com.dawgpizza.menu.pizzas)
        );
		       $(".veg" ).html(
            vegTemplate(com.dawgpizza.menu.pizzas)
        );
		       $(".desserts" ).html(
            dessertTemplate(com.dawgpizza.menu.desserts)
        );
		
//Filling in the dranks
var drinks = "";
for(var i = 0; i < com.dawgpizza.menu.drinks.length; i++){
	if(i == com.dawgpizza.menu.drinks.length - 1){
		drinks = drinks + com.dawgpizza.menu.drinks[i].name;	
	}else {
	drinks = drinks + com.dawgpizza.menu.drinks[i].name + ", ";
	}
}
drinks = drinks + " all you can drink for" + com.dawgpizza.menu.drinks[0].price;

$( "li.dranks" ).html(drinks);
		});